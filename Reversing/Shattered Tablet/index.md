# Shattered Tablet

Running the program prompts the user to enter what the tablet says. There are no immediate hints.

# Solution

Disassembling the program using Ghidra yields more useful findings.

There is a code block in main that I could reference to manually reconstruct the flag by following the pattern of characters.

![Disassembling the program in Ghidra](./1-ghidra-disassembly.png)

# Flag

`HTB{br0k3n_4p4rt,n3ver_t0_b3_r3p41r3d}`