# She Shells C Shells

## Testing the Program

When first running the program the user is presented with a shell that has a handful of commands including `getflag`:

![Testing program](./1-testing-program.png)

Running getflag prompts the user for a password, but we don't know what it is at this point.

## Disassembly

Viewing the disassembly in Ghidra the `getflag` command calls the `func_flag` function.

![Viewing disassembly in Ghidra](./2-viewing-disassembly.png)

The password provided by the user (`local_118`) is encrypted by an xor function using `m1` and is compared to byte string `t` stored in the program using memcmp. If the byte strings match, it runs a separate xor function with a different key `m2`.

![Byte string for t variable](./2a-t-byte-string.png)

The byte string for `t` is: `2c4ab799a3e57078936e97d9476d38bdffbb85996fe14aab74c37ba8b29fd7ecebcd63b23923e184929609c699f258facb6f6f5e1fbe2b138ea5a99993ab8f701cc0c43ea6fe933590c3c910e9`.

![Byte string for m1 variable](./2b-m1-byte-string.png)

The 1st xor encryption key `m1` is:
`6e3fc3b9d78d1558e50ffbac224d57dbdfcfedfc1c846ad81ca617c4c1bfa08587a143d4584f8da8b2f27ca3b98637dabf070a7e73df5c60aecacfb9e0deff0070b9e45fc89ab351f5aea87e8d`

![Byte string for m2 variable](./2c-m2-byte-string.png)

The 2nd xor encryption key `m2` is `641ef5e2c097441bf85ff9be185d488e91e4f6f15c8d269e2ba102f7c6f7e4b398fe57ed4a4bd1f6a1eb09c699f258facb6f6f5e1fbe2b138ea5a99993ab8f701cc0c43ea6fe933590c3c910e9`.

## Solution

Using CyberChef, we can decode `t` using `m1` to peek at the getflag password.

![Decrypting getflag command password](./3-decrypting-getflag-password.png)

And we can also decode `t` using `m2` to find the flag itself.

![Decrypting the flag](./4-decrypting-flag.png)

## Flag

Running the program and providing this password `But the value of these shells will fall, due to the laws of supply and demand` retrieves the flag confirming our solution.

![Obtaining flag through getflag command](./5-solution.png)

`HTB{cr4ck1ng_0p3n_sh3ll5_by_th3_s34_sh0r3}`