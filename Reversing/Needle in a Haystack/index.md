# Needle in a Haystack

I was definitely overthinking on this challenge initially. Originally I loaded the program into Ghidra and was trying to find the flag in the assembly.

## Solution

This challenge serves as a good reminder that initial recon such as running the strings command (`strings haystack`) can help make quick work of easier challenges.

![strings command output](./1-strings-command-output.png)

## Flag

`HTB{d1v1ng_1nt0_th3_d4tab4nk5}`