# Hunting License

## Disassembly

Opening up the challenge in Ghidra we see the main() function calls exam() if the user proceeds to take the test. Then the user needs to provide 3 correct answers to pass the exam.

![Disassembling the program in Ghidra](./1-disassembly.png)

As seen in the screenshot of the disassembly, the first password is `PasswordNumeroUno`.

The next two passwords aren't this easy and it might be easiest to debug the program using gdb and examine the values stored in memory when the program calls strcmp.

![Assembly and address of second password strcmp function call](./1a-2nd-strcmp-call.png)

![Assembly and address of third password strcmp function call](./1b-3rd-strcmp-call.png)

I made a note of the addresses in exam() that call strcmp:
* 0x401316
* 0x40138a

## Debugging with gdb

Running the program with gdb and setting a breakpoint on the second strcmp function in exam() using `b *0x401316` helps us obtain the second password easily `P4ssw0rdTw0`.

![Using gdb to obtain 2nd password](./2-gdb-debugging-2nd-password.png)

Doing the same for the third function using `b *0x40138a`.

![Using gdb to obtain 3rd password](./3-gdb-debugging-3rd-password.png)

It looks like the final password is `ThirdAndFinal!!!`.

## Solution

Now I connect to the docker container for the challenge and answer a quiz using netcat to obtain the flag:

```
What is the file format of the executable?
> ELF 
[+] Correct!

What is the CPU architecture of the executable?
> amd64
[+] Correct!

What library is used to read lines for user answers? (`ldd` may help)
> libreadline.so.8
[+] Correct!

What is the address of the `main` function?
> 0x401172
[+] Correct!

How many calls to `puts` are there in `main`? (using a decompiler may help)
> 5
[+] Correct!

What is the first password?
> PasswordNumeroUno
[+] Correct!

What is the reversed form of the second password?
> 0wTdr0wss4P
[+] Correct!

What is the real second password?
> P4ssw0rdTw0
[+] Correct!

What is the XOR key used to encode the third password?
> 19
[+] Correct!

What is the third password?
> ThirdAndFinal!!!
[+] Correct!

[+] Here is the flag: `HTB{l1c3ns3_4cquir3d-hunt1ng_t1m3!}`
```

![Docker Container Questions and Answers](./4-docker-q-and-a.png)

# Flag

`HTB{l1c3ns3_4cquir3d-hunt1ng_t1m3!}`