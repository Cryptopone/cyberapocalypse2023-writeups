# Reconfiguration

To get this one solved I had to install a tool called [Orange Data Mining](https://orangedatamining.com/). This tool was able to open and read the analysis.ows file.

From there I needed to link the file to the scatter plot. Once I viewed the scatter plot I was able to see the flag.

![Viewing the scatterplot](./1-viewing-the-scatterplot.png)

## Flag

`HTB{sc4tter_pl0ts_4_th3_w1n}`