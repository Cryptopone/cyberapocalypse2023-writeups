# Persistence

This challenge requires you to visit the remote docker server and retrieve the flag by making a GET request to /flag.

![Most requests result in a failure](./1-flag-failure.png)

However, the odds of this appearing is apparently 1 out of 1000 times.

![Once in a while the flag is shown](./1-flag-success.png)

I ended up using Burp Intruder (Professional version) to iterate the endpoint. The flag happened to be the largest response length at 216 and yielded the flag.

![Obtaining the flag with Burp Intruder](./2-burp-intruder-flag.png)

## Flag

`HTB{y0u_h4v3_p0w3rfuL_sCr1pt1ng_ab1lit13S!}`