# Restricted

When a user ssh's into this machine normally they are stuck inside of a restricted shell.

## Solution

This took some trial and error but one of the payloads from [HackTricks](https://book.hacktricks.xyz/linux-hardening/privilege-escalation/escaping-from-limited-bash) helped me obtain a regular shell.

`ssh -p <PORT> -t restricted@<IP-ADDRESS> bash`

![Restricted shell bypass](./1-solution.png)

## Flag

`HTB{r35tr1ct10n5_4r3_p0w3r1355}`