# Remote Computation

This challenge involves connecting to a docker container and quickly answering 500 math questions. With that said, the server does not give you any time to manually calculate the answer even if you wanted to slowly answer all 500 questions.

## Testing the Program

![Testing the program manually](./1-testing-program.png)

## Solution

Fortunately, I was able to quickly put together a pwntools script to do the work for me. After 500 questions, the flag is provided.

```python
import pwn

p = pwn.remote("0.0.0.0", "1337")

print(p.recv().decode("utf-8"))

p.sendline(b"1")


currentIteration = int(p.recvuntil(b"]:").decode("utf-8")[-5:-2])

while currentIteration <= 500:
    print(currentIteration)
    
    currentQuestion = p.recvuntil(b">").decode("utf-8")[:-5]
    print(currentQuestion)
    try:
        result = eval(currentQuestion)
        if -1337.00 <= result <= 1337.00:
            print(str(round(result,2)))
            p.sendline(b"" + bytes(str(round(result,2)), encoding="utf8"))
        else:
            p.sendline(b"MEM_ERR")
    except ZeroDivisionError:
        p.sendline(b"DIV0_ERR")
    except SyntaxError:
        p.sendline(b"SYNTAX_ERR")

    if currentIteration >= 500:
        break

    currentIteration = int(p.recvuntil(b"]:").decode("utf-8")[-5:-2])
    
print(p.recvall().decode("utf-8"))
```

![Running the solution script](./2-solution-script.png)

## Flag

`HTB{d1v1d3_bY_Z3r0_3rr0r}`