# Shooting 101

## Initial Setup

I downloaded the challenge files and spawn the docker containers for the challenge. Then I verified the rpc endpoint works and used netcat to obtain the connection information.

![Obtaining the smart contract details](./1-smart-contract-details.png)

```
Private key     :  0xa90559d8b505a838b979e1e4e724d419fcd79d50b398cd6f1238b16dd7a64ec1
Address         :  0x660e0552bCb65Cf8Fa9623a629D99e7884D385CF
Target contract :  0xa5f1cfFf8c0E79F4fbD14E0270Ca439cB7bF5D03
Setup contract  :  0x67C2237bCf4eCf0860e555EEFfB341eA2E74A696
```

## The Challenge

I have already setup Foundry-rs in the first challenge (Navigating the Unknown) so I can focus on hacking the smart contracts for this one.

### Setup.sol

![Setup.sol sourcecode](./2-setup-sol-source.png)

### Unknown.sol

![Unknown.sol sourcecode](./3-unknown-sol-source.png)

Now I need to learn a bit more about the solidity language to understand how these contracts were implemented.

- A payable function allows someone to send ether to a contract and run code to account for the deposit [reference](https://docs.alchemy.com/docs/solidity-payable-functions).
- Function modifiers are used to modify the behavior of a function [reference](https://www.tutorialspoint.com/solidity/solidity_function_modifiers.htm).
- A contract can have at most one receive function that is declared using `receive() external payable`. This is used by the smart contract to receive ether [reference](https://docs.soliditylang.org/en/v0.8.17/contracts.html).
- Smart contracts can have at most one fallback function. The fallback function is used when none of the other functions match the given function signature, or if no data was provided and the contract does not have a receive function [reference](https://docs.soliditylang.org/en/v0.8.17/contracts.html#fallback-function).

## First Target

I'm thinking we need to call firstTarget, secondTarget and thirdTarget then verify we've completed the task via the TCP connection.

Calling the firstTarget will be counter-intuitive, as you essentially need to call something that doesn't exist to avoid reaching the secondTarget instead. The modifiers ensure the order is being followed.

### Shooting the First Target

```
export TARGET_CONTRACT=0xa5f1cfFf8c0E79F4fbD14E0270Ca439cB7bF5D03
export PRIVATE_KEY=0xa90559d8b505a838b979e1e4e724d419fcd79d50b398cd6f1238b16dd7a64ec1
cast send $TARGET_CONTRACT "invalid()" --rpc-url http://165.232.98.11:31396/ --private-key $PRIVATE_KEY
```

### Returns

```
blockHash               0xd579a68c9679a26db1efe1f6a5605436da628f06b120bb09744f46ba08d388f6
blockNumber             2
contractAddress         
cumulativeGasUsed       43764
effectiveGasPrice       3000000000
gasUsed                 43764
logs                    []
logsBloom               0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
root                    
status                  1
transactionHash         0x0ccdc647f03b2cf250f177941010a287fe928190f28b61ccc2b86d03c0062272
transactionIndex        0
type                    2
```

![Shooting the first target](./4-shooting-first-target.png)

## Second Target

Now to call the receive() function with no arguments.

### Shooting the Second Target

```
cast send $TARGET_CONTRACT --rpc-url http://165.232.98.11:31396/ --private-key $PRIVATE_KEY
```

### Returns

```
blockHash               0x416d55474344972f97f3e88f611731b6fce81bb9660223aec50fc13f9fbdbfd9
blockNumber             3
contractAddress         
cumulativeGasUsed       26492
effectiveGasPrice       3000000000
gasUsed                 26492
logs                    []
logsBloom               0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
root                    
status                  1
transactionHash         0x2f300ac0e09d82b3d74a0c2f8ad56e5840188a088dc9eda72eba16e6668281ed
transactionIndex        0
type                    2
```
![Shooting the second target](./5-shooting-second-target.png)

## Third Target

And finally we can call third().

### Shooting the Third Target

```
cast send $TARGET_CONTRACT "third()" --rpc-url http://165.232.98.11:31396/ --private-key $PRIVATE_KEY
```

### Returns

```
blockHash               0xe321d910bd6c8aa750d5a14acb829193c8959443769c607c2432b2e83c00a667
blockNumber             4
contractAddress         
cumulativeGasUsed       26620
effectiveGasPrice       3000000000
gasUsed                 26620
logs                    []
logsBloom               0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
root                    
status                  1
transactionHash         0x930687d0747f8b29b38b63eb5637adfdf424d9b4d2427dfd06ac655a1497ab4d
transactionIndex        0
type                    2
```

![Shooting the third target](./6-shooting-third-target.png)

## Retrieving the Flag

Checking the TCP connection we confirm we can obtain the flag.

![Retrieving the flag](./7-retrieve-flag.png)

## Flag

`HTB{f33l5_n1c3_h1771n6_y0ur_74r6375}`