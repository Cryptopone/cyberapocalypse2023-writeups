# Navigating the Unknown

This challenge was interesting because I have not really interacted with smart contracts before.

## Initial Setup

The files in this challenge includes a README to help get the user going. It suggests using `web3py`, `web3js` or `foundry-rs` (a dedicated CLI) to interact with the blockchain. I opted to try and get going with foundry-rs even if documentation on it is sporadic.

I then found [this](https://www.youtube.com/watch?v=fNMfMxGxeag) YouTube video to help familiarize myself with what I was getting into. I use [this](https://github.com/foundry-rs/foundry) official looking repo as a reference. I then reviewed the bash script at https://foundry.paradigm.xyz and then ran the code in a shell to perform the initial install.

Once that was done I opened up VSCode and loaded the challenge folder. Then used the integrated terminal to run `foundryup` to complete the install. This got me up and running to do the challenge.

## The Challenge

Two docker instances are spawned for this challenge. One is the rpc server that can be accessed using http://. The other was a tcp endpoint that provides the Setup and Target addresses and will give us the flag if we complete the task. The tcp endpoint was accessed using netcat.

![Obtaining the smart contract details](./1-initial-connection-details.png)

Taking a quick look at the two solidity files:

### Setup.sol

![Setup smart contract details](./2-setup-sol.png)

### Unknown.sol

![Unknown smart contract details](./3-unknown-sol.png)

## Interacting With Smart Contracts

Foundry-rs installs two tools:
- forge (Allows us to build test and deploy smart contracts)
- cast (Allows us to execute on-chain transactions and interact with deployed smart contracts)

It was clear for this challenge I'd need to use the cast command. The Setup contract essentially checks Unknown's version and if it's 10 the TCP endpoint will give us the flag.

First thing I tried was confirm connectivity to the rpc server. I found the documentation to cast [here](https://book.getfoundry.sh/reference/cast/cast-rpc). Then I followed one of the examples and used cast's rpc command to retrieve the latest block number.
`cast rpc --rpc-url http://159.65.94.38:31872/ eth_getBlockByNumber "latest" "false"`

![Confirming I can reach the smart contract](./4-initial-rpc-connectivity-check.png)

This worked and helped reassure me I set everything up correctly. Now to figure out how to update the Unknown smart contract.

## Solution

Through a bit of trial and error (and following [this page](https://book.getfoundry.sh/reference/cast/cast-send) I was able to interact with my first smart contract:
```bash
export TARGET_CONTRACT=0x696a76E7742cCB18B5DACa47a592570F3c5a18d5
cast send $TARGET_CONTRACT "updateSensors(uint256)" 10 --rpc-url http://159.65.94.38:31872/ --private-key=0x09b6d4e081be649ba99486cfaa011105853d1fdab61029880ba64c1b76aa1707
```

![Calling a smart contract function](./5-cast-command-update-sensors.png)

Checking in with the TCP connection I was able to retrieve the flag:

![Retrieving the flag](./6-retrieve-flag.png)

## Flag

`HTB{9P5_50FtW4R3_UPd4t3D}`