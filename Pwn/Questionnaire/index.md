# Questionnaire

This challenge requires us to connect to a remote docker container and answer a quiz to retrieve the flag. There is also a `test` program that we will reference when answering the questions.

## The Challenge

![The challenge text](./1-challenge-text.png)

## Questionnaire

1. Is this a '32-bit' or '64-bit' ELF? (e.g. 1337-bit)

Running readelf -h test on the file reveals it's a 64-bit program.

```
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              EXEC (Executable file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x401090
  Start of program headers:          64 (bytes into file)
  Start of section headers:          14152 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         13
  Size of section headers:           64 (bytes)
  Number of section headers:         31
  Section header string table index: 30
```

2. What's the linking of the binary? (e.g. static, dynamic)

Running `file test` reveals the binary is `dynamic`.

```
test: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=5a83587fbda6ad7b1aeee2d59f027a882bf2a429, for GNU/Linux 3.2.0, not stripped
```

3. Is the binary 'stripped' or 'not stripped'?

From the previous question output we know the file is `not stripped`.

4. Which protections are enabled (Canary, NX, PIE, Fortify)

We can use a tool called checksec to see protections on an ELF binary `checksec --file=test` to see the file has `NX`.
```
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH	Symbols		FORTIFY	Fortified	Fortifiable	FILE
Partial RELRO   No canary found   NX enabled    No PIE          No RPATH   No RUNPATH   40 Symbols	  No	0		1		test
```

5. What is the name of the custom function that gets called inside 'main()'? (e.g. vulnerable_function())

`vuln`

6. What is the size of the 'buffer' (in hex or decimal)?

`0x20`

7. Which custom function is never called? (e.g. vuln())?

`gg`

8. What is the name of the standard function that could trigger a Buffer Overflow? (e.g. fprintf())?

`fgets`

9. Insert 30, then 39, then 40 'A's in the program and see the output. After how many bytes a Segmentation Fault occurs (in hex or decimal)?

`40`

10. What is the address of 'gg()' in hex? (e.g. 0x401337)

Running `objdump -D -F test | grep gg` reveals `0x401176`:
```
0000000000401176 <gg> (File Offset: 0x1176):
```

Answering these we obtain the flag.

![Retrieving the flag](./2-flag.png)

## Flag

`HTB{th30ry_bef0r3_4cti0n}`
