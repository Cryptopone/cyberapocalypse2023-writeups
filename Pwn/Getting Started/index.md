# Getting Started

Running the `gs` program provided with the challenge walks through the buffer overflow process. I manually overflowed the buffer using 40 `A` characters and 8 `B` characters.

![Overflowing the buffer manually](./1-manual-overflow.png)

I also installed pwntools and updated the payload line to 44 `A` characters to obtain the flag via the script.

![Overflowing the buffer using pwntools script](./2-pwntools-overflow.png)

## Flag

`HTB{b0f_s33m5_3z_r1ght}`
