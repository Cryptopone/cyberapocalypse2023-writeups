# Initialise Connection

This challenge requires us to connect to a remote docker container and press `1` to obtain the flag.

![Challenge Text](./1-challenge-text.png)

![Get the flag](./2-getting-flag.png)

## Flag

`HTB{g3t_r34dy_f0r_s0m3_pwn}`