# Ancient Encodings

Reviewing the source of the python script the data is base 64 encoded, then the byte array is converted to long, and output in hex.

## Challenge Sourcecode

```python
from Crypto.Util.number import bytes_to_long
from base64 import b64encode

FLAG = b"HTB{??????????}"


def encode(message):
    return hex(bytes_to_long(b64encode(message)))


def main():
    encoded_flag = encode(FLAG)
    with open("output.txt", "w") as f:
        f.write(encoded_flag)


if __name__ == "__main__":
    main()
```

## Solution

Cyberchef can be used to quickly convert from hex and decode the base64 output to obtain the flag.

![CyberChef Decode](./1-cyberchef-decode.png)

## Flag

`HTB{1n_y0ur_j0urn3y_y0u_wi1l_se3_th15_enc0d1ngs_ev3rywher3}`