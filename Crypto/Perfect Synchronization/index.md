# Perfect Synchronization

This one took me some time to figure out, but it ended up being a good puzzle.

## Challenge Sourcecode

```python
from os import urandom
from Crypto.Cipher import AES
from secret import MESSAGE

assert all([x.isupper() or x in '{_} ' for x in MESSAGE])


class Cipher:

    def __init__(self):
        self.salt = urandom(15)
        key = urandom(16)
        self.cipher = AES.new(key, AES.MODE_ECB)

    def encrypt(self, message):
        return [self.cipher.encrypt(c.encode() + self.salt) for c in message]


def main():
    cipher = Cipher()
    encrypted = cipher.encrypt(MESSAGE)
    encrypted = "\n".join([c.hex() for c in encrypted])

    with open("output.txt", 'w+') as f:
        f.write(encrypted)


if __name__ == "__main__":
    main()
```

## Investigation

After looking at the sourcecode I realized the algorithm is using ECB which is an insecure mode, but even worse is only one character is encrypted at a time (the 15 byte salt and 16 byte key are static). First I needed to create a list of distinct hashes and count their occurrances, then I could translate the hashes to ASCII values and use this [quipqiup](https://quipqiup.com/) tool to perform some analysis.

### Translate.py

```python
def main():
    # load up the original cipher text
    file = open('output_orig.txt', 'r')
    ciphertext_fromfile = file.readlines()
    file.close()

    # declare a new ciphertext dict to count the occurrances of each character
    ciphertext = {}
    for each in ciphertext_fromfile:
        each = each.split('\n')[0]
        if each not in ciphertext.keys():
            ciphertext.update({each: 1})
        else:
            ciphertext.update({each: ciphertext[each]+1})

    # Sort the dictionary by occurrance in descending order
    sortedcipher = dict(reversed(sorted(ciphertext.items(), key=lambda item: item[1])))

    # Write the dictionary to file
    file = open('output_stats.txt', "w")
    for each in sortedcipher.keys():
        file.write("{ciphertext}\t{frequency}\n".format(ciphertext=each, frequency=ciphertext[each]))
    file.close()

if __name__ == "__main__":
    main()
```

### Reclass.py

Then I put together another python script to translate these hashes to characters based on the most common english characters [reference](https://www3.nd.edu/~busiforc/handouts/cryptography/letterfrequencies.html).

```python
def main():
    file = open('output_orig.txt', 'r')
    ciphertext_fromfile = file.readlines()
    file.close()

    file = open('output_reclass.txt', "w")
    for each in ciphertext_fromfile:
        if each[:-1] == '61331054d82aeec9a20416759766d9d5':
            file.write(" ")
        if each[:-1] == 'c87a7eb9283e59571ad0cb0c89a74379':
            file.write("E")
        if each[:-1] == '200ecd2657df0197f202f258b45038d8':
            file.write("A")
        if each[:-1] == '5f122076e17398b7e21d1762a61e2e0a':
            file.write("R")
        if each[:-1] == '68d763bc4c7a9b0da3828e0b77b08b64':
            file.write("I")
        if each[:-1] == '305d4649e3cb097fb094f8f45abbf0dc':
            file.write("O")
        if each[:-1] == 'e9b131ab270c54bbf67fb4bd9c8e3177':
            file.write("T")
        if each[:-1] == '34ece5ff054feccc5dabe9ae90438f9d':
            file.write("N")
        if each[:-1] == '8cbd4cfebc9ddf583a108de1a69df088':
            file.write("S")
        if each[:-1] == 'f89f2719fb2814d9ab821316dae9862f':
            file.write("L")
        if each[:-1] == '457165130940ceac01160ac0ff924d86':
            file.write("C")
        if each[:-1] == '3a17ebebf2bad9aa0dd75b37a58fe6ea':
            file.write("U")
        if each[:-1] == 'd178fac67ec4e9d2724fed6c7b50cd26':
            file.write("D")
        if each[:-1] == 'e23c1323abc1fc41331b9cdfc40d5856':
            file.write("P")
        if each[:-1] == '5d7185a6823ab4fc73f3ea33669a7bae':
            file.write("M")
        if each[:-1] == 'dfc8a2232dc2487a5455bda9fa2d45a1':
            file.write("H")
        if each[:-1] == '2190a721b2dcb17ff693aa5feecb3b58':
            file.write("G")
        if each[:-1] == '78de2d97da222954cce639cc4b481050':
            file.write("B")
        if each[:-1] == '9673dbe632859fa33b8a79d6a3e3fe30':
            file.write("F")
        if each[:-1] == '4a3af0b7397584c4d450c6f7e83076aa':
            file.write("Y")
        if each[:-1] == '66975492b6a53cc9a4503c3a1295b6a7':
            file.write("W")
        if each[:-1] == 'fb78aed37621262392a4125183d1bfc9':
            file.write("K")
        if each[:-1] == '0df9b4e759512f36aaa5c7fd4fb1fba8':
            file.write("V")
        if each[:-1] == '293f56083c20759d275db846c8bfb03e':
            file.write("X")
        if each[:-1] == '60e8373bfb2124aea832f87809fca596':
            file.write("Z")
        if each[:-1] == '2fc20e9a20605b988999e836301a2408':
            file.write("_")
        if each[:-1] == 'a94f49727cf771a85831bd03af1caaf5':
            file.write("Q")
        if each[:-1] == '5ae172c9ea46594cea34ad1a4b1c79cd':
            file.write("_")
        if each[:-1] == 'fbe86a428051747607a35b44b1a3e9e9':
            file.write("{")
        if each[:-1] == 'c53ba24fbbe9e3dbdd6062b3aab7ed1a':
            file.write("}")


    file.close()

if __name__ == "__main__":
    main()
```

## Solution

Now I can take my `output_reclass.txt' contents and run them through quipqiup:

```
HOEZDENCM RNRLMATA TA FRAEP SN IUE HRCI IURI TN RNM BTVEN AIOEICU SH WOTIIEN LRNBDRBE CEOIRTN LEIIEOA RNP CSYFTNRITSNA SH LEIIEOA SCCDO WTIU VROMTNB HOEZDENCTEA YSOESVEO IUEOE TA R CURORCIEOTAITC PTAIOTFDITSN SH LEIIEOA IURI TA OSDBULM IUE ARYE HSO RLYSAI RLL ARYGLEA SH IURI LRNBDRBE TN COMGIRNRLMATA HOEZDENCM RNRLMATA RLAS KNSWN RA CSDNITNB LEIIEOA TA IUE AIDPM SH IUE HOEZDENCM SH LEIIEOA SO BOSDGA SH LEIIEOA TN R CTGUEOIEXI IUE YEIUSP TA DAEP RA RN RTP IS FOERKTNB CLRAATCRL CTGUEOA HOEZDENCM RNRLMATA OEZDTOEA SNLM R FRATC DNPEOAIRNPTNB SH IUE AIRITAITCA SH IUE GLRTNIEXI LRNBDRBE RNP ASYE GOSFLEY ASLVTNB AKTLLA RNP TH GEOHSOYEP FM URNP ISLEORNCE HSO EXIENATVE LEIIEO FSSKKEEGTNB PDOTNB WSOLP WRO TT FSIU IUE FOTITAU RNP IUE RYEOTCRNA OECODTIEP CSPEFOERKEOA FM GLRCTNB COSAAWSOP GD__LEA TN YRJSO NEWAGRGEOA RNP ODNNTNB CSNIEAIA HSO WUS CSDLP ASLVE IUEY IUE HRAIEAI AEVEORL SH IUE CTGUEOA DAEP FM IUE RXTA GSWEOA WEOE FOERKRFLE DATNB HOEZDENCM RNRLMATA HSO EXRYGLE ASYE SH IUE CSNADLRO CTGUEOA DAEP FM IUE JRGRNEAE YECURNTCRL YEIUSPA SH LEIIEO CSDNITNB RNP AIRITAITCRL RNRLMATA BENEORLLM UIF{RQATYGLEQADFAITIDITSNQTAQWERK} CROP IMGE YRCUTNEOM WEOE HTOAI DAEP TN WSOLP WRO TT GSAATFLM FM IUE DA ROYMA ATA ISPRM IUE UROP WSOK SH LEIIEO CSDNITNB RNP RNRLMATA URA FEEN OEGLRCEP FM CSYGDIEO ASHIWROE WUTCU CRN CROOM SDI ADCU RNRLMATA TN AECSNPA WTIU YSPEON CSYGDITNB GSWEO CLRAATCRL CTGUEOA ROE DNLTKELM IS GOSVTPE RNM OERL GOSIECITSN HSO CSNHTPENITRL PRIR GD__LE GD__LE GD__L
```

![quipqiup](./1-quipqiup.png)

## Flag

I wasn't looking for style points and while it didn't get the flag exactly correct, I was able to fix it up easily.

`HTB{A_SIMPLE_SUBSTITUTION_IS_WEAK}`