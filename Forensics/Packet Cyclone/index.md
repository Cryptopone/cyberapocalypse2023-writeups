# Packet Cyclone

The zip file contains an export of a bunch of windows event logger categories along with two rclone sigma rules in yaml files.

The sigma rules [reference](https://research.nccgroup.com/2021/05/27/detecting-rclone-an-effective-tool-for-exfiltration/) a research page that created these rules based on forensics.

## Investigation

My search begins in powershell event viewer file to see if I can find anything.

I noticed a time jump within the log file where the clock was rolled back:

![Noticed time rollback](./1-system-time-rollback.png)

The security event log also makes mention to this:

![Confirming system time was rolled back](./2-confirmed-system-time-rollback.png)

We also see someone has logged into the `wade` user account and several security groups (including Administrators) report being enumerated at this time:

![User login and account enumeration](./3-user-login-and-user-enumeration.png)

Windows Firewall shows a network profile was changed from None to Public for an ethernet adapter:

![Windows Firewall profile change](./4-windows-firewall-profile-changed.png)

Very suspicious finding in Microsoft-Windows-Sysmon%4Operational

![Initial R-Clone usage](./5-rclone-usage.png)

![Data exfiltration via R-Clone](./6-rclone-data-exfiltration.png)

## Questions and Answers

It was at this point I realized I needed to connect to the docker instance to answer the questions and obtain the flag.

1. What is the email of the attacker used for the exfiltration process?

```
majmeret@protonmail.com
```

2. What is the password for the attacker used for the exfiltration process?

```
FBMeavdiaFZbWzpMqIVhJCGXZ5XXZI1qsU3EjhoKQw0rEoQqHyI
```

3. What is the Cloud storage provider used by the attacker?

```
mega
```

4. What is the ID of the process used by the attackers to configure their tool?

```
3820
```

5. What is the name of the folder the attacker exfiltrated; provide the full path.

```
C:\Users\Wade\Desktop\Relic_location
```

6. What is the name of the folder the attacker exfiltrated the files to?

```
exfiltration
```

By answering the questions correctly we receive the flag.

![Connecting to docker questionnaire to complete the challenge](./7-questions-answers-and-flag.png)

## Flag

`HTB{3v3n_3xtr4t3rr3str14l_B31nGs_us3_Rcl0n3_n0w4d4ys}`