# Plaintext Treasure

We are given a pcap file to go through with wireshark. It didn't take long to find the HTTP stream with the flag in plaintext.

![Flag in plaintext HTTP request](./1-plaintext-flag.png)

## Flag

`HTB{th3s3_4l13ns_st1ll_us3_HTTP}`