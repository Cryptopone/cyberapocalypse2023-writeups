# Alien Cradle

This one is a simple powershell script and it doesn't take long to see the flag as a concatenated string

![Concatenated flag](./1-flag.png)

## Flag

`HTB{p0w3rsh3ll_Cr4dl3s_c4n_g3t_th3_j0b_d0n3}`