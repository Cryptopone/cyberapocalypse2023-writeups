# Roten

This challenge is investigating a busy pcap file.

## Initial Findings

The attacker was using wfuzz against the server, making a lot of noise in the logs. I found it helpful to filter out `http.response.code != 404`. This was when it was easier to spot several 500 Internal Server Error codes in the logs:

![Spotting a 500 Internal Server Error](./1-spotting-500-error.png)

The last 500 response confirmed the attacker was using a web shell:

![Confirming a web shell was used](./2-confirming-web-shell-usage.png)

## Narrowing Down Attack Vector

I changed my approach to search for `http.request.method == POST` to see if the attacker attempted to upload data to the server. Sure enough, one of the last post requests was using Content-Type application/x-php

![Applying a new filter](./3-applying-filters.png)

And viewing the HTTP conversation I confirmed they uploaded obfusicated php code to the server

![Finding the webshell code](./4-discovering-uploaded-shell.png)

## Deobfusication

I loaded the code into VSCode and began to review. After auditing the code, there were a few lines at the bottom that decrypted the payload, and opted to print out the output instead of passing it to eval():

![Reviewing and deobfusicating the code](./5-review-and-deobfusication.png)

## Flag

Printing the output of the code (instead of running it) and finding the flag as a comment.

![Finding the flag as a comment](./6-discovering-the-flag.png)

`##flag = HTB{W0w_ROt_A_DaY}`