# Gunhead

This website has 3 buttons on the righthand side of the page with the bottom one being a `Command` button.

Opening it up we get a console and are told to enter `/help` for all commands.

![List of help commands](./1-help-commands.png)

One of the commands is ping.


## Solution

Viewing the sourcecode, we confirm the IP address is passed into shell_exec without any sanitization which could lead to command injection.

![Sourcecode of ping command](./2-ping-source-code.png)

We can simply run `/ping 127.0.0.1; cat ../flag.txt` and observe the response to obtain the flag.

![POST response with flag](./3-post-response-with-flag.png)

## Flag

`HTB{4lw4y5_54n1t1z3_u53r_1nput!!!}`