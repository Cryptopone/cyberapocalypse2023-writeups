# Spybug

Loading the website presents us with a login page.

![Initial login page](./1-login-page.png)

## Investigation

I started by reviewing the agent sourcecode (`agent/spybug-agent.go`) and obtained `/agents/register` and `agents/checkin/:id/:token` endpoints.

![Agent register endpoint](./2-agent-register-endpoint.png)

![Agent check endpoint](./3-agent-check-endpoint.png)

## Researching the Server

To better understand the objective here I loaded up a local instance of the web server and set the admin password to `admin` so I could login.

This is where I spotted the flag contained in the webpage.

![Researching admin login](./4-admin-login.png)

The template to render the page is susceptible to html injection when the agents supply details but we are stopped due to the CSP policy `script-src 'self'`:

![Spotting the HTML injection bug](./5-html-injection-bug.png)

![Attack fails due to CSP policy](./5a-stopped-by-CSP.png)

## Finding a Bug in the Challenge?

The `spybug-agent.go` file demonstrates how an agent uploads to the server. I ran the agent and captured the HTTP traffic using Wireshark but was surprised to see the server responded with a 400 error.

```
POST /agents/upload/7439ed17-6f11-403d-8728-d62c13be348b/54909a67-7a80-4744-9c63-d557814c29b7 HTTP/1.1
Host: 127.0.0.1:1337
User-Agent: Go-http-client/1.1
Content-Length: 244896
Content-Type: multipart/form-data; boundary=b16cca1e92c04a197ead046ef7b7a5101cb878c42728a5c1a8429b40cd1e
Accept-Encoding: gzip

--b16cca1e92c04a197ead046ef7b7a5101cb878c42728a5c1a8429b40cd1e
Content-Disposition: form-data; name="recording"; filename="rec.wav"
Content-Type: application/octet-stream

RIFF....WAVEfmt ........D...D.......data.....................................................................................
<truncated remainder of data>
--b16cca1e92c04a197ead046ef7b7a5101cb878c42728a5c1a8429b40cd1e--

HTTP/1.1 400 Bad Request
X-Powered-By: Express
Content-Security-Policy: script-src 'self'; frame-ancestors 'none'; object-src 'none'; base-uri 'none';
Cache-Control: no-cache, no-store, must-revalidate
Pragma: no-cache
Expires: 0
Content-Type: text/plain; charset=utf-8
Content-Length: 11
ETag: W/"b-EFiDB1U+dmqzx9Mo2UjcZ1SJPO8"
Set-Cookie: connect.sid=s%3AsSURe5VJBSlzLHryfI5QZITKyGd6OWze.oG1SDt8BW%2B8bRfJDot1qnX%2BxL23p01ycNhFho8bCgEo; Path=/; HttpOnly
Date: Wed, 22 Mar 2023 17:34:29 GMT
Connection: keep-alive
Keep-Alive: timeout=5

Bad Request
```

The server side code does a fair number of checks on the uploaded file as well:

![Checking how an upload is verified](./6a-agent-server-side-checks.png)

![Checking mimetype and filename validation](./6b-agent-server-side-checks.png)

This is where I noticed two things:

1. The server expects an `audio/wave` file format but my agent sent `application/octet-stream`. This explains the 400 error and resending this request in BurpSuite Repeater with this fix allows the upload to go through.
1. The regex used to confirm the data type has a small error in that this data must be present in the file (it doesn't need to start with this data).

So when I was first playing around with this I was able to send arbitrary data as the file content.

![Making a successful upload](./7-successful-uploads.png)

## Formulating an Attack

Uploaded files can be retrieved from `http://localhost:1337/uploads/acf1d538-52cc-4461-8585-ef36bf1ec432` by referencing the id that was provided to us for a successful upload. Here we see the Content-Type is `application/octet-stream`. This mime type is a bit funny because the client may decide to detect it differently.

![Retrieving a file upload](./8-retrieving-file-upload.png)

With this knowledge I crafted an attack and leveraged Burp Collaborator:

1. Upload a file containing raw javascript with `RIFF....WAVEfmt` commented out.
2. Initiate an XSS by leveraging `/agents/details/:identifier/:token` and supplying a `<script src=> tag referencing the uploaded file from the previous step. This bypasses the `script-src 'self'` CSP check.

![Uploading a file with malicious payload](./9a-upload-malicious-file.png)

![Performing HTML injection attack and reference upload](./9b-perform-html-injection.png)

## XSS to Victory

The challenge has an admin bot that logs in to check the panel every so often. I waited for Collaborator to register the hit, and to see the flag.

![XSS attack gives us the flag](./10-xss-to-flag.png)

## Flag

`HTB{p01yg10t5_4nd_35p10n4g3}`