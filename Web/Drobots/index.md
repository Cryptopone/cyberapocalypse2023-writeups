# Drobots

This challenge contains a flask application with a database.

## Investigation

Auditing the `database.py` file in the sourcecode suggests the application may be vulnerable to an SQL Injection attack as username and password are concatenated into the SQL statement.

![Reviewing database.py](./1-database-py-audit.png)

## Solution

Navigating to the website reveals a login page.

We can execute an SQL Injection attack with the following credentials:
* Username: `admin`
* Password: `admin" or "1" = "1`

Once logged in the flag is visible in the Robot Manufacturer column.

![Logging in and seeing the flag](./2-logged-in-flag.png)

## Flag

`HTB{p4r4m3t3r1z4t10n_1s_1mp0rt4nt!!!}`