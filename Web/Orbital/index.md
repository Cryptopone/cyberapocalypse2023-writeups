# Orbital

The website presents us with a login page but there is no sign up functionality.

![Initial login page](./1-initial-page-load.png)

## Investigation

Reviewing the source code I see an opportunity in `application/database.py` in the `login` function. SQL injection can be done on the username field as the parameter is concatenated to the SQL statement.

![Spotting SQL injection](./2-reviewing-source-code.png)

I also review `application/util.py` to see how passwords are checked when logging in and see it's an MD5 hash. It should be possible to union a record by setting username=`admin` and password to a hash that we can generate.

Next I review `application/blueprints/routes.py` and spot an `/export` endpoint that will read files from the filesystem.

![Export endpoint](./2a-review-export-api-endpoint.png)

It looks like path traversal is possible here allowing us to read files outside of the application.

Finally, the `Dockerfile` shows the flag is located at `/signal_sleuth_firmware`.

![Flag location in docker image](./2b-flag-location.png)

## Solution

First, I generate an MD5 hash for `password`.

![Generating MD5 hash with CyberChef](./4-generate-md5-hash.png)

Now I send a POST request to `/api/login` with the following body

```json
{
    "username":"apples\" union all select \"username\", \"5f4dcc3b5aa765d61d8327deb882cf99",
    "password":"password"
}
```

This request is successful and allows us to authenticate to the server.

![Logging in with union all](./5-sql-injection-via-union-all.png)

Now I am able to reach `/api/export` and obtain the flag via `../signal_sleuth_firmware`.

```json
{
    "name":"../signal_sleuth_firmware"
}
```
![Obtaining the flag using export](./6-obtain-flag.png)

## Flag

`HTB{T1m3_b4$3d_$ql1_4r3_fun!!!}`

Note: I'm guessing this is an alternate solution as I didn't go the timed route and forged my own credentials instead :P