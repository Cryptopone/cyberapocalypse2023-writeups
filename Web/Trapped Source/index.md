# Trapped Source

Users are presented with a webpage containing a keypad.

Viewing the sourcecode of the webpage we see the correctPin: `8291` located within a script tag.

![Page Source Code](./1-source-code.png)

Entering this as the code and pressing `Enter` yields the flag.

![Correct Code](./2-flag.png)

## Flag

`HTB{V13w_50urc3_c4n_b3_u53ful!!!}`