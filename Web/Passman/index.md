# Passman

The website presents a login page and an option to create an account.

![Initial Webpage](./1-initial-load.png)

## Investigation

After creating an account and logging in, the application appears to be a password manager. It's also worth mentioning it's not possible to create an account using `admin`, as the error message is rather descriptive.

```
Error: ER_DUP_ENTRY: Duplicate entry 'admin' for key 'username'
```

![After logging in](./2-logged-in.png)

Adding a new password shows the POST request uses graphql.

![Reviewing POST request to add a password](./3-review-graphql-query.png)

It's interesting to see the id returned is 7, suggesting 1-6 may already be in use.

## Code Review

Since the challenge provides access to the source code I reviewed `helpers/GraphqlHelper.js` and noticed an `UpdatePassword` function.

![Looking at UpdatePassword](./4-code-review-graphqlhelper.png)

The mutation doesn't appear to perform any validation allowing us to craft a request and impersonate the admin user.

## Solution

To solve the challenge, I crafted a GraphQL mutation to call UpdatePassword with username=`admin` and password=`password`.

```json
{"query":"mutation($username: String!, $password: String!) { UpdatePassword(username: $username, password: $password) {message } }",
"variables":{
"username":"admin",
"password":"password"}
}
```

![Updating the admin password via GraphQL](./5-update-password.png)

At this point I was able to login as the `admin` user and could see the flag.

![Getting the flag as admin](./6-obtain-flag.png)

## Flag

`HTB{1d0r5_4r3_s1mpl3_4nd_1mp4ctful!!}`