# Timed Transmission

The challenge file provided contains a .sal extension. Searching for `hardware .sal signal file` led me to find a signal processing program called [Logic 2](https://support.saleae.com/logic-software/sw-download) by Saleae.

## Flag

Once installed, I opened the file and zoomed in and could see the signal was the flag itself. Here you can see `HTB{b3`

![image](./1-initial-file-load.png)

HTB{b391N_tH3_HArdWAr3_QU3St}