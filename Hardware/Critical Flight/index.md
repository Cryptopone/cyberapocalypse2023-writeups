# Critical Flight

The project file contains a number of gbr (Gerber) files.

I was able to find a program called [KiCad](https://kicad.org) allowing me to view the files.

## Flag

The flag was split between two files:
- HadesMicro-B_Cu.gbr
 
![PCB - Flag Side 1](./1-pcb-flag.png)

- HadesMicro-In1_Cu.gbr

![PCB - Flag Side 2](./2-pcb-flag.png)

HTB{533_7h3_1nn32_w02k1n95_0f_313c720n1c5#$@}