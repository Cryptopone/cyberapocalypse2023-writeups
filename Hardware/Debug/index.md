# Debug

This challenge presents a `hw_debug.sal` file containing the flag. I needed to leverage [Logic 2](https://support.saleae.com/logic-software/sw-download) by Saleae to see if I can find it.

## Initial View

Loading up the file we see two channels but only the Receive (RX) channel contains signal data.

![Initial view](./1-initial-view.png)

## Solution

To solve this I had to learn to use one of the built-in analyzers for the program: `Async Serial`.

I changed the following:
* Input Channel: `01. RX`
* Bit Rate (Bits/s): `115200`
* Bits per Frame: `8 Bits per Transfer (Standard)`
* Stop Bits: `1 Stop Bit (Standard)`
* Parity Bit: `No Parity Bit (Standard)`
* Significant Bit: `Least Significant Bit Sent First (Standard)`
* Signal inversion: `Non Inverted (Standard)`
* Mode: `Normal`
* Show in protocol results table: `checked`
* Stream to terminal: `checked`

![Configure Async Serial](./2-configure-async-serial.png)

When I saved the settings, sure enough the signals are translated to text and I am able to read the output:

![Initial debug output text](./3-initial-debug-output-text.png)

Scrolling down to the bottom, we can see the flag is printed at the end of several debug lines:

## Flag

![Flag](./4-debug-flag.png)

`HTB{547311173_n37w02k_c0mp20m153d}`