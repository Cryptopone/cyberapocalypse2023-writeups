# CyberApocalypse 2023 CTF Writeups

A collection of writeups from HackTheBox's [Cyber Apocalypse 2023 - The Cursed Mission](https://ctf.hackthebox.com/event/details/cyber-apocalypse-2023-the-cursed-mission-821) CTF.

The event took place between Mar 18, 2023 - Mar 23, 2023.

## Ranking

This year I solved 35 out of 74 challenges and my team placed 299th as a solo participant.

## Welcome Flag:

`HTB{l3t_th3_tr3asur3_hunt1ng_b3g1n!}`

## Pwn

* [Initialise Connection](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Pwn/Initialise%20Connection)
* [Questionnaire](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Pwn/Questionnaire)
* [Getting Started](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Pwn/Getting%20Started)
* [Labyrinth](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Pwn/Labyrinth)

## Web

* [Trapped Source](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Trapped%20Source)
* [Gunhead](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Gunhead)
* [Drobots](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Didactic%20Octo%20Paddles)
* [Passman](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Passman)
* [Orbital](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Orbital)
* [Didactic Octo Paddles](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Didactic%20Octo%20Paddles)
* [SpyBug](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Web/Spybug)

## Blockchain

* [Navigating the Unknown](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Blockchain/Navigating%20the%20Unknown)
* [Shooting 101](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Blockchain/Shooting%20101)
* [The Art of Deception](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Blockchain/The%20Art%20of%20Deception)

## Hardware

* [Timed Transmission](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Hardware/Timed%20Transmission)
* [Critical Flight](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Hardware/Critical%20Flight)
* [Debug](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Hardware/Debug)

## Reversing

* [Shattered Tablet](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Reversing/Shattered%20Tablet)
* [She Shells C Shells](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Reversing/She%20Shells%20C%20Shells)
* [Needle in a Haystack](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Reversing/Needle%20in%20a%20Haystack)
* [Hunting License](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Reversing/Hunting%20License)

## ML

* [Reconfiguration](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/ML/Reconfiguration)

## Misc

* [Persistence](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Misc/Persistence)
* [Hijack](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Misc/Hijack)
* [Restricted](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Misc/Restricted)
* [Remote computation](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Misc/Remote%20Computation)

## Forensics

* [Plaintext Treasure](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Forensics/Plaintext%20Treasure)
* [Alien Cradle](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Forensics/Alien%20Cradle)
* [Extraterrestrial Persistence](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Forensics/Extraterrestrial%20Persistence)
* [Roten](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Forensics/Roten)
* [Packet Cyclone](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Forensics/Packet%20Cyclone)

## Crypto

* [Ancient Encodings](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Crypto/Ancient%20Encodings)
* [Small Steps](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Crypto/Small%20Steps)
* [Perfect Synchronization](https://gitlab.com/Cryptopone/cyberapocalypse2023-writeups/-/tree/main/Crypto/Perfect%20Synchronization)